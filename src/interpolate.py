input_loc = 'input/'
output_loc = 'output/'

framerate = 24
target_framerate = 30
window = 4
overlap = 2
start_frame = 0
max_frames = 480

convolutionThreshold = 5000

import sys, math, copy

import numpy
import scipy, scipy.signal, scipy.integrate
import scipy.ndimage.interpolation

from dpx import *
from laplacian_like import *
from riesz import *

with open(input_loc + "{:07d}".format(start_frame) + '.dpx','rb') as f:
	meta = readDPXMetaData(f)

data = [[],[],[]]
bases = []
newdata = [[],[],[]]
newbases = []
stretchfactor = float(target_framerate)/float(framerate)
framecount = 0
output_framecount = 0
done = False

trimLeft = 60
trimRight = 60
trimTop = 68
trimBottom = 68

print()
cfc = framecount

while True:
	

	for kk in range(3):
		for ii in range(len(data[kk])):
			for jj in ['A','phi']:
				if jj in data[kk][ii]:
					data[kk][ii][jj] = numpy.roll(data[kk][ii][jj], window, axis=2)
		if (kk < len(bases)):
			bases[kk] = numpy.roll(bases[kk], window, axis=2)
	
	print('loading frame      ',end="",flush=True)
	while (framecount < cfc + window + overlap):
		print('\b\b\b\b\b{:05d}'.format(framecount),end="",flush=True)
		try:
			f = open(input_loc + "{:07d}".format(start_frame + framecount) + '.dpx','rb')
		except:
			print("\nFile Error")
			sys.exit(1)
		frame = readDPXImageData(f, meta)[trimTop:meta['height']-trimBottom,trimLeft:meta['width']-trimRight,:]
		f.close()

		for kk in range(3):
			pyr = build_laplacian(frame[:,:,kk], minsize=4, convolutionThreshold=convolutionThreshold)
			rieszpyr = laplacian_to_riesz(pyr)
			polar_pyr = riesz_to_spherical(rieszpyr)
			for jj in ['A','phi']:
				for ii in range(len(polar_pyr[jj])):
					if (ii >= len(data[kk])):
						data[kk].append({})
						newdata[kk].append({})
					if (jj not in data[kk][ii]):
						data[kk][ii][jj] = numpy.empty((polar_pyr[jj][ii].shape[0],polar_pyr[jj][ii].shape[1],overlap * 2 + window))
						newdata[kk][ii][jj] = numpy.empty((data[kk][ii][jj].shape[0], data[kk][ii][jj].shape[1], int((window + 2*overlap) * stretchfactor)))
					data[kk][ii][jj][:,:,framecount - cfc + overlap] = polar_pyr[jj][ii]
			if kk >= len(bases):
				bases.append(numpy.empty((polar_pyr['base'].shape[0],polar_pyr['base'].shape[1],overlap * 2 + window)))
				newbases.append(numpy.empty((polar_pyr['base'].shape[0],polar_pyr['base'].shape[1],int((window + 2*overlap) * stretchfactor))))
			bases[kk][:,:,framecount - cfc + overlap] = polar_pyr['base']

		framecount += 1
		if (framecount == max_frames):
			done = True
			break

	longoverlap = int(overlap * stretchfactor)

	print('\nprocessing frames')
	for kk in range(3):
		for jj in ['A','phi']:
			for ii in range(len(data[kk])):
				#if (framecount == window + overlap):
				#	newdata[kk][ii][jj][:,:,longoverlap:] = scipy.signal.resample(data[kk][ii][jj][:,:,overlap:], int((window + overlap) * stretchfactor), axis=2)
				#else:
				#	newdata[kk][ii][jj][:,:,:] = scipy.signal.resample(data[kk][ii][jj], int((window + 2*overlap) * stretchfactor), axis=2)
				xx, yy, zz = numpy.meshgrid(range(data[kk][ii][jj].shape[1]), range(data[kk][ii][jj].shape[0]), range(int((window + 2*overlap) * stretchfactor)), copy=False)
				idx = numpy.empty((3, xx.shape[0], xx.shape[1], xx.shape[2]), dtype=numpy.float64)
				idx[0,:,:,:] = numpy.float64(yy)
				idx[1,:,:,:] = numpy.float64(xx)
				idx[2,:,:,:] = numpy.float64(zz)/stretchfactor
				scipy.ndimage.interpolation.map_coordinates(data[kk][ii][jj], idx, output=newdata[kk][ii][jj])
		xx, yy, zz = numpy.meshgrid(range(bases[kk].shape[1]), range(bases[kk].shape[0]), range(int((window + 2*overlap) * stretchfactor)), copy=False)
		idx = numpy.empty((3, xx.shape[0], xx.shape[1], xx.shape[2]), dtype=numpy.float64)
		idx[0,:,:,:] = numpy.float64(yy)
		idx[1,:,:,:] = numpy.float64(xx)
		idx[2,:,:,:] = numpy.float64(zz)/stretchfactor
		scipy.ndimage.interpolation.map_coordinates(bases[kk], idx, output=newbases[kk])


	print('exporting frame      ',end="",flush=True)	
	for jj in range(min(int(stretchfactor* window), int(stretchfactor * (framecount - cfc)))):
		print('\b\b\b\b\b{:05d}'.format(output_framecount),end="",flush=True)
		recon = numpy.zeros((meta['height'], meta['width'],3), dtype=numpy.float64)
		for kk in range(3):
			currentframe = {'A':[], 'phi':[], 'base':newbases[kk][:,:,jj+longoverlap]}
			for ii in range(len(newdata[kk])):
				currentA = newdata[kk][ii]['A'][:,:,jj+longoverlap]
				currentPhi = newdata[kk][ii]['phi'][:,:,jj+longoverlap]
				
				#currentA = scipy.signal.convolve(scipy.signal.convolve(currentA, gaussiankernel.T, mode='same'), gaussiankernel, mode='same')
				currentframe['A'].append(currentA)

				#denom = scipy.signal.convolve(scipy.signal.convolve(currentA, gaussiankernel.T, mode='same'), gaussiankernel, mode='same')
				#denom_zero = (denom == 0)
				#denom = numpy.where(denom_zero, 1, denom)
				#currentPhi = numpy.where(denom_zero, 0, scipy.signal.convolve(scipy.signal.convolve(currentA * currentPhi, gaussiankernel.T, mode='same'), gaussiankernel, mode='same') / denom)
				currentframe['phi'].append(currentPhi)


			derieszpyr = riesz_spherical_to_laplacian(currentframe)
			recon[trimTop:meta['height']-trimBottom,trimLeft:meta['width']-trimRight,kk] = collapse_laplacian(derieszpyr)
		with open(output_loc + "{:07d}".format(output_framecount) + '.dpx','wb') as f:
			writeDPX(f, recon, meta)
		output_framecount += 1

	print()
	cfc = framecount - overlap


	if done:
		break